﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using MySql.Data.MySqlClient;
using System.Windows.Forms;

namespace BooksMySql
{
    public partial class FormMain : Form
    {
        private MySqlConnection connectionSql;
        private MySqlCommand cmdSql;


        public FormMain()
        {
            InitializeComponent();

            string strConnection = "Server=127.0.0.1;Port=3306;User=root;Password=1234;Database=library";

            connectionSql = new MySqlConnection(strConnection);

            cmdSql = connectionSql.CreateCommand();
        }

        private void FormMain_Load(object sender, EventArgs e)
        {
            try
            {
                connectionSql.Open();

                SelectingAllValuesInTheTableBooks();
            }
            catch (Exception error)
            {
                connectionSql.Close();

                MessageBox.Show(error.Message, "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            /*finally
            {
                connectionSql.Close();

                this.Close();
            }*/
        }

        private void FormMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            connectionSql.Close();
        }

        private void SelectingAllValuesInTheTableBooks()
        {
            dataGridViewBooks.Rows.Clear();

            cmdSql.CommandText = "SELECT * FROM books;";

            MySqlDataReader readerSql = cmdSql.ExecuteReader();

            while (readerSql.Read())
            {
                dataGridViewBooks.Rows.Add(
                    readerSql.GetInt32("ArticleCD"),
                    readerSql.GetString("Title"),
                    readerSql.GetString("Author"),
                    readerSql.GetInt32("PublicationDate")
                    );
            }

            readerSql.Close();
        }

        private void textBoxPublicationDate_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)48 && e.KeyChar <= (char)57 && textBoxPublicationDate.TextLength < 4) || e.KeyChar == (char)Keys.Back)
            {
                if (e.KeyChar == (char)Keys.D0 && textBoxPublicationDate.TextLength == 0)
                {
                    e.Handled = true;
                }

                return;
            }

            e.Handled = true;
        }

        private void textBoxAuthor_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)1040 && e.KeyChar <= (char)1103 && textBoxAuthor.TextLength < 80) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                if (e.KeyChar == (char)Keys.Space && textBoxAuthor.TextLength == 0)
                {
                    e.Handled = true;
                }

                if (textBoxAuthor.TextLength != 0)
                {
                    if (e.KeyChar == (char)Keys.Space && textBoxAuthor.Text[textBoxAuthor.TextLength - 1] == (char)Keys.Space)
                    {
                        e.Handled = true;
                    }
                }

                return;
            }

            e.Handled = true;
        }

        private void textBoxTitle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= (char)1040 && e.KeyChar <= (char)1103 && textBoxTitle.TextLength < 255) || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                if (e.KeyChar == (char)Keys.Space && textBoxTitle.TextLength == 0)
                {
                    e.Handled = true;
                }

                if (textBoxTitle.TextLength != 0)
                {
                    if (e.KeyChar == (char)Keys.Space && textBoxTitle.Text[textBoxTitle.TextLength - 1] == (char)Keys.Space)
                    {
                        e.Handled = true;
                    }
                }

                return;
            }

            e.Handled = true;
        }

        private void buttonAddBook_Click(object sender, EventArgs e)
        {
            if (CheckingCompletionValues())
            {
                for (int i = 0; i < dataGridViewBooks.Rows.Count; i++)
                {
                    if ((int)(dataGridViewBooks.Rows[i].Cells[0].Value) == Convert.ToInt32(textBoxCD.Text))
                    {
                        MessageBox.Show("Не уникальная запись!");

                        return;
                    }
                }

                cmdSql.CommandText = $"INSERT INTO books (Title, Author, PublicationDate) VALUES ('{textBoxTitle.Text}', '{textBoxAuthor.Text}', {textBoxPublicationDate.Text});";

                try
                {
                    cmdSql.ExecuteNonQuery();

                    SelectingAllValuesInTheTableBooks();
                }
                catch (Exception error)
                {
                    connectionSql.Close();

                    MessageBox.Show(error.Message, "Предупреждение!", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }

                ClearingInputValues();
            }
        }

        private void ClearingInputValues()
        {
            textBoxCD.Clear();
            textBoxTitle.Clear();
            textBoxPublicationDate.Clear();
            textBoxAuthor.Clear();
        }

        private void dataGridViewBooks_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridViewBooks.SelectedRows.Count != 0)
            {
                DataGridViewRow row = dataGridViewBooks.SelectedRows[0];

                textBoxCD.Text = row.Cells[0].Value.ToString();
                textBoxTitle.Text = row.Cells[1].Value.ToString();
                textBoxAuthor.Text = row.Cells[2].Value.ToString();
                textBoxPublicationDate.Text = row.Cells[3].Value.ToString();
            }
        }

        private void buttonDelete_Click(object sender, EventArgs e)
        {
            if (textBoxCD.TextLength != 0)
            {
                try
                {
                    cmdSql.CommandText = $"DELETE FROM books WHERE ArticleCD = {textBoxCD.Text};";

                    cmdSql.ExecuteNonQuery();

                    SelectingAllValuesInTheTableBooks();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);

                    connectionSql.Close();

                    ClearingInputValues();
                }

                ClearingInputValues();
            }
            else
            {
                MessageBox.Show("Не выбрана строка для удаления!");
            }
        }

        private bool CheckingCompletionValues()
        {
            if (textBoxTitle.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле: Название!");

                return false;
            }
            else if (textBoxAuthor.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле: Автор!");

                return false;
            }
            else if (textBoxPublicationDate.TextLength == 0)
            {
                MessageBox.Show("Не заполнено поле: Год публикации!");

                return false;
            }
            else
            {
                return true;
            }
        }

        private void buttonUpdate_Click(object sender, EventArgs e)
        {
            if (textBoxCD.TextLength != 0 && CheckingCompletionValues())
            {
                try
                {
                    cmdSql.CommandText = $"UPDATE books SET Title = '{textBoxTitle.Text}', Author = '{textBoxAuthor.Text}', PublicationDate = {textBoxPublicationDate.Text} WHERE ArticleCD = {textBoxCD.Text};";

                    cmdSql.ExecuteNonQuery();

                    SelectingAllValuesInTheTableBooks();
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);

                    connectionSql.Close();

                    ClearingInputValues();
                }

                ClearingInputValues();
            }
            else if (textBoxCD.TextLength == 0)
            {
                MessageBox.Show("Не выбрана строка для обновления!");
            }
        }
    }
}
